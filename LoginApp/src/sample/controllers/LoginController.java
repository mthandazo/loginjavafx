package sample.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;

import javax.sound.sampled.Line;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LoginController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane loginAnch;

    @FXML
    private AnchorPane loginRanch;

    @FXML
    private ImageView loginImage;

    @FXML
    private Rectangle loginRect;

    @FXML
    private Label loginTitle;

    @FXML
    private Label loginWelcome;

    @FXML
    private JFXTextField loginUserInput;

    @FXML
    private JFXPasswordField loginPassInput;

    @FXML
    private JFXButton loginBtn;

    @FXML
    private Hyperlink loginReg;

    @FXML
    private Line loginLeftLine;

    @FXML
    private Line loginRightLine;

    @FXML
    private Label loginOr;

    @FXML
    void loadSignUp(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/sample/views/register.fxml"));
            Scene scene = loginBtn.getScene();
            root.translateXProperty().set(scene.getWidth());
            loginRanch.getChildren().add(root);

            Timeline timeline = new Timeline();
            KeyValue kv = new KeyValue(root.translateXProperty(), 0, Interpolator.EASE_IN);
            KeyFrame kf = new KeyFrame(Duration.seconds(1), kv);
            timeline.getKeyFrames().add(kf);
            timeline.setOnFinished(event1 -> {
                loginRanch.getChildren().remove(loginAnch);
            });
            timeline.play();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @FXML
    void initialize() {
        loginUserInput.setStyle("-fx-text-inner-color: #ffffff");
        loginPassInput.setStyle("-fx-text-inner-color: #ffffff");

        loginBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.println("Hello from the Login Controller !!");
            }
        });
    }
}
