package sample.controllers;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class RegisterController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private AnchorPane regRanch;

    @FXML
    private AnchorPane regAnch;

    @FXML
    private JFXTextField regUsername;

    @FXML
    private JFXTextField regEmail;

    @FXML
    private JFXPasswordField regPass;

    @FXML
    private JFXButton regBtn;

    @FXML
    private Hyperlink regLgn;

    @FXML
    void loadSignIn(ActionEvent event) throws IOException {
        switchIntent();
    }

    @FXML
    void initialize() {
        regUsername.setStyle("-fx-text-inner-color: #ffffff");
        regEmail.setStyle("-fx-text-inner-color: #ffffff");
        regPass.setStyle("-fx-text-inner-color: #ffffff");

        regBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                System.out.printf("Hello from the Register Controller!!");
            }
        });
    }

    private void switchIntent() throws IOException{
        regBtn.getScene().getWindow().hide();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/views/login.fxml"));
        loader.load();

        Parent root = loader.getRoot();
        Stage signInStage = new Stage();
        signInStage.setScene(new Scene(root));
        signInStage.setResizable(false);
        signInStage.show();

    }


}
